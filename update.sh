#! /bin/bash
# System update
yay -Syu

# Copy the kernel upgrade in the boot directory
sudo cp /boot/vmlinuz-linux /boot/efi/EFI/arch/vmlinuz-linux.efi
sudo cp /boot/initramfs-linux.img /boot/efi/EFI/arch/initramfs-linux.img
sudo cp /boot/initramfs-linux-fallback.img /boot/efi/EFI/arch/initramfs-linux-fallback.img

# To be able to verify the kernel's upgrade
ls -l /boot/
ls -l /boot/efi/EFI/arch/
