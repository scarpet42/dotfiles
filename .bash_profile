#############################################################
#  Auteur : Stéphane CARPENTIER
#  Fichier : .bash_profile
#    Modif : mer. 03 juil. 2024 22:22
#
#############################################################

# The package lines ----------------------------------------- {{{

[[ -f ~/.bashrc ]] && . ~/.bashrc

# End of the package lines ---------------------------------- }}}

# slrn personalisation -------------------------------------- {{{

# To be used by slrn
NNTPSERVER='news.free.fr' && export NNTPSERVER

# End of slrn personalisation ------------------------------- }}}

# Bash personalisation -------------------------------------- {{{

# I don't want lines twice in my .bash_history
HISTCONTROL=ignoredups && export HISTCONTROL
HISTIGNORE="alias:bat:cat:dtf:du:fd:lc:ll:ls:/bin/ls:lsblk:man:md:nvim:psc:us:rd:sda:wd:zd" && export HISTIGNORE

# To have a huge .bash_history
HISTSIZE=50000 && export HISTSIZE

# End of bash personalisation ------------------------------- }}}

# My preferred editors -------------------------------------- {{{

# To use neovide and not Emacs by default
export VISUAL="neovide -f"
export EDITOR="nvim -f"

# To get colors in man pages
export PAGER="most"

# End of editors' choice ------------------------------------- }}}

if [ -e /home/stef/.nix-profile/etc/profile.d/nix.sh ]; then . /home/stef/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# the graphical session -------------------------------------- {{{

# To get the variables
  export QT_QPA_PLATFORM=wayland
  export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
  export XKB_DEFAULT_RULES=evdev
  export XKB_DEFAULT_MODEL=pc105
  export XKB_DEFAULT_LAYOUT=fr
  export XKB_DEFAULT_VARIANT=oss
  export XKB_DEFAULT_OPTIONS=caps:super
  # Since version 2.2.1 it doesn't work without it
  export WEBKIT_FORCE_SANDBOX=0
# To launch the VM at login only on tty1
# See if it's the best later
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  #exec startx
  exec sway
fi

# End of graphical session ---------------------------------- }}}

# To have fun with guix ------------------------------------- {{{
GUIX_PROFILE="$HOME/.guix-profile"
. "$GUIX_PROFILE/etc/profile"
GUIX_PROFILE="$HOME/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"
# End of fun with Guix -------------------------------------- }}}

# vim: ts=4 sw=4 noet

