"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Auteur : Stéphane CARPENTIER
"  Fichier : init.vim
"    Modif : dim. 21 janv. 2024 14:14
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Managing nvim dependencies-------------- {{{
" Python 2
let g:python_host_prog  = '/usr/bin/python2'
let g:python3_host_prog  = '/usr/bin/python'
let g:loaded_python_provider = 0
" It sucks to do it like that
let g:ruby_host_prog = '/home/stef/.gem/ruby/3.0.0/bin/neovim-ruby-host'
" Managing nvim dependencies-------------- }}}

" Managing the plugins -------------- {{{

" This enables automatic indentation in writing.
:filetype indent on

" To have plugins depending on the filetype
:filetype plugin on

" To manage the plugins not managed by the system with Plug
:call plug#begin('~/.config/nvim/plugged')
:	Plug 'luochen1990/rainbow'
:	Plug 'vlime/vlime', {'rtp': 'vim/'}
:	Plug 'Olical/conjure'
:	Plug 'https://gitlab.com/HiPhish/guile.vim'
:	Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" Launch ':TSInstall python' for first used
:call plug#end()
" To install the plugin managed launch the command :
" :PlugInstall

" END of the plugin management ---------- }}}

" If I'm in neovide
		" The default settings -------------- {{{

		" It's GUI in Linux I choose the font and that's where the fun begins
		" Set the font name
		":let g:sca_font_name='DejaVu Sans Mono'
		":let g:sca_font_name='Droid Sans Mono'
		":let g:sca_font_name='FontAwesome'
		":let g:sca_font_name='Liberation Mono'
		":let g:sca_font_name='Monospace'
		:let g:sca_font_name="Source\ Code\ Pro"
		":let g:sca_font_name="DejaVuSansMono\ Nerd\ Font"
		"set guifont=DejaVuSansMono\ Nerd\ Font:h18
		" The tricky part is for the size
		" Minimal size expected for a font
		:let g:sca_font_size_min=10
		" Maximal size expected for a font
		:let g:sca_font_size_max=48
		" Initial size of the font
		:let g:sca_font_size_ini=16
		" Set the font size with the initial size
		:let g:sca_font_size_actual=g:sca_font_size_ini
		" Set the default font used
		:let &guifont=g:sca_font_name . ':h' . g:sca_font_size_actual
		":set guifont=g:sca_font_name . ':h' . g:sca_font_size_actual

		" END of defaults settings ------------------ }}}

		" Defining the functions to [in|de]crease the size ---------- {{{

		:function ScaIncreaseSize()
		" If the size isn't greater than the max allowed let's increase it
			:if g:sca_font_size_actual < g:sca_font_size_max
				:let g:sca_font_size_actual = g:sca_font_size_actual + 1
				:let &guifont=g:sca_font_name . ':h' . g:sca_font_size_actual
			:endif
		:endfunction

		:function ScaDecreaseSize()
		" If the size isn't inferior to the min allowed let's decrease it
			:if g:sca_font_size_actual > g:sca_font_size_min
				:let g:sca_font_size_actual = g:sca_font_size_actual - 1
				:let &guifont=g:sca_font_name . ':h' . g:sca_font_size_actual
			:endif
		:endfunction

		:function ScaReinitSize()
		" Reinitialize the font size as it started
			:let g:sca_font_size_actual = g:sca_font_size_ini
			:let &guifont=g:sca_font_name . ':h' . g:sca_font_size_actual
		:endfunction

		" END of the function definition ------------------- }}}


		" Mapping the functions for easy use ---------------- {{{
		" I could have used a leader, but I prefer the same defaults as other
		" tools like zathura, termite and qutebrowser : it's easier to
		" learn/use it it doesn't seemed used. Be careful, the mapping can be
		" changed if you have an querty keyboard
		:noremap <C--> :call ScaDecreaseSize()<cr>
		:inoremap <C--> <esc>:call ScaDecreaseSize()<cr>a
		:noremap <C-+> :call ScaIncreaseSize()<cr>
		":noremap <C-S-+> :call ScaIncreaseSize()<cr>
		:inoremap <C-+> <esc>:call ScaIncreaseSize()<cr>a
		":inoremap <C-S-+> <esc>:call ScaIncreaseSize()<cr>a
		:noremap <C-=> :call ScaReinitSize()<cr>
		:inoremap <C-=> <esc>:call ScaReinitSize()<cr>a

		" END of mapping ------------------ }}}


" For nvim capacities --------------- {{{
" To use the system clipboard without needing to call it
set clipboard+=unnamedplus
" For nvim capacities --------------- }}}

" Settings of Environment specifications ------------------- {{{

" To use the visual mode automatically "*
:set guioptions+=a
" I don't use a mouse, I don't need a menu.
:set guioptions-=m
:set guioptions-=g
:set guioptions-=t
:set guioptions-=e
" I don't need an icon bar for the same reason.
:set guioptions-=T
:set guioptions-=i
" I don't need a scrollbar for the same reason.
:set guioptions-=r
:set guioptions-=L
:set guioptions-=l
:set guioptions-=R
" To see if it's better
:set guioptions+=p

" I can use the mouse, mostly in Windows at work. It doesn't hurt to put this
" option and it can help when needed. So even if it's exceptional, I put it.
:set mouse=a

" END of the WM specifications -------------- }}}

" Settings depending on the filetype -------------- {{{

" File settings for Vim type ---------- {{{

:augroup FileType_Vim
:	autocmd!
:	autocmd Filetype vim setlocal foldmethod=marker
:	autocmd Filetype vim setlocal foldlevel=0
:augroup END

" End of vim settings ----------- }}}

" File settings for Bash type ---------- {{{

:augroup FileType_Bash
:	autocmd!
:	autocmd Filetype sh setlocal foldmethod=marker
:	autocmd Filetype sh setlocal foldlevel=0
:augroup END

" End of bash settings ----------- }}}

" File settings for python type ---------- {{{

:augroup FileType_Python
:	autocmd!
:	autocmd Filetype python setlocal foldmethod=marker
:	autocmd Filetype python setlocal foldlevel=0
:	autocmd Filetype python TSEnable python
:augroup END

" End of python settings ----------- }}}

" File settings for clisp type ---------- {{{

:augroup FileType_CLisp
:	autocmd!
" The connection to the server
:	autocmd Filetype lisp map <F9> <LocalLeader>rr
" Compile stuff in the window
:	autocmd Filetype lisp map <F2> <LocalLeader>of
" Execute stuff under the cursor
:	autocmd Filetype lisp map <F3> <LocalLeader>ss
" Execute selected stuff in visual mode
:	autocmd Filetype lisp map <F4> <LocalLeader>s
" Rainbow parenthesis
:	autocmd Filetype lisp let g:lisp_rainbow=1
:augroup END

" End of scheme settings ----------- }}}

:augroup FileType_Scheme
:	autocmd!
" Open the logs
:	autocmd Filetype scheme map <F9> <LocalLeader>ls
" Execute inner bloc
:	autocmd Filetype scheme map <F2> <LocalLeader>ee
" Execute global block
:	autocmd Filetype scheme map <F3> <LocalLeader>er
" Execute global block
:	autocmd Filetype scheme map <F4> viw<LocalLeader>E
" Rainbow parenthesis
:	autocmd Filetype scheme let g:lisp_rainbow=0
" To use Guile socket, comment it to use MIT-scheme socket
:	autocmd Filetype scheme let g:conjure#filetype#scheme = "conjure.client.guile.socket"
:	autocmd Filetype scheme let g:conjure#client#guile#socket#pipename = "/home/stef/.guile-repl.socket"
" To connect to the socket
:	autocmd Filetype scheme map <F10> <LocalLeader>cc
:augroup END

" End of scheme settings ----------- }}}

" File settings for config files type ---------- {{{

:augroup FileType_Config
:	autocmd!
:	autocmd Filetype conf setlocal foldmethod=marker
:	autocmd Filetype conf setlocal foldlevel=0
:augroup END

" End of config files  settings ----------- }}}

" File settings for LaTeX Files ---------- {{{
" It's mostly important for Windows
:augroup Filetype_LaTeX
:	autocmd!
"	The latexmain files must be recognised as LaTeX files
:	autocmd BufRead *.latexmain set filetype=vim
"	To be able to work with Windows
:	autocmd Filetype tex setlocal  shellslash
" 	IMPORTANT: grep will sometimes skip displaying the file name if you
" 	search in a singe file. This will confuse Latex-Suite. Set your grep
" 	program to always generate a file-name.
:	autocmd Filetype tex set grepprg=grep\ -nH\ $*
"	OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
"	'plaintex' instead of 'tex', which results in vim-latex not being loaded.
"	The following changes the default filetype back to 'tex':
:	autocmd Filetype tex let g:tex_flavor='latex'
"	The foldlevel for LaTeX files to see for the future
:	autocmd BufRead *.tex set foldlevel=0
:augroup END

" To be able to search the word under the cursor in duckduckgo. Qutebrowser
" must be installed first. Or put another browser. It could have been set in
" the related mutt/slrn part even if I'm using it mostly with LaTeX
:noremap <C-G> :!qutebrowser <cword> >& /dev/null &<CR><CR>

" End of settings for LaTeX files ---------- }}}

" Settings related to the mutt/slrn use ----------- {{{

" All the files in the folder .mutt must be config files
:autocmd BufRead ~/.mutt/* set filetype=muttrc

" To justify text in mutt/slrn
:source $VIMRUNTIME/macros/justify.vim
" The command select the paragraph on which is the cursor and justify it
:nnoremap <leader>f :set expandtab<CR>vipgqvip_j:set noexpandtab<CR>

" End of mutt/slrn related settings --------------- }}}

" END of settings depe,dent on the filetype ------- }}}

" Settings useful with parenthesis, braquets, whatever ---------- {{{

" Very useful when playing woth a lot of parenthesis.
" To colorize the parenthesis level :
:let g:rainbow_active = 1
" To change switch the rainbow
:nnoremap <leader>rt :RainbowToggle<cr>
" I don't understand why it's not by default. It's useful with the next option
:set matchpairs+=<:>
" It's great to be able to see what parenthesis is closed by the one I write.
:set showmatch

" End of parenthesis settings ---------- }}}

" Settings of the status line ----------------- {{{

" Use of airline to have a nice statusline.
" Uncomment only the theme to use, use :AirlineTheme to try the themes
"let g:airline_theme='dracula'
let g:airline_theme='night_owl'
":let g:airline_theme='soda'
":let g:airline_theme='nord'

" The powerline's fonts are nicer but need to have powerline installed
:let g:airline_powerline_fonts = 1
"let g:Powerline_symbols = 'fancy'

" To always display the statusline
:set laststatus=2

" END of status line configuration --------------- }}}

" settings related to the cursor -------------- {{{

" Those two settings or designed to get the number of the ligne on which is
" the cursor and to get the relative position of the other lines
:set relativenumber
:set number
" To highlight the cursor's line
:set cursorline

" To use the backspace key
:set backspace=indent,eol,start
" I know, the arrows are bad, but go on the next visual line when a long line
" is wraped
:noremap <Up> gk
:noremap <Down> gj
" To allow the keys to move to the next/previous line
:set whichwrap=b,s,<,>,[,],h,l

" Mostly, I don't want automatic returns at the end of the lines
:set textwidth=0
" To always see five lines before and after the cursor. To get a better
" context except on the firsts and lasts lines.
:set so=5

" END of settings related to the cursor --------------- }}}

" Settings of the search --------------- {{{

" To highlight the results of the search.
:set hlsearch
" Very useful to see if the pattern is good and to see if we have to press
" more keys for the search to be useful. We don't want to wait for the [ENTER]
" key to be pressed.
:set incsearch
" To hide easily the result when useless and to display it again when needed.
:noremap <C-F1> :set hlsearch!<cr>
" It's great to be able to say very easily if we want the search to be case
" sensitive or not.
:set ignorecase smartcase
" To display the number of results matching the search
:set shortmess -=S

" To be able to find the files more easily with :find
" Probably better to add only some directories, but it will do for now
:set path+=$PWD/**

" END of search settings ----------------- }}}

" Settings of the wrapped lines ------------ {{{

" Don't wrap the lines anyway. Especially not in the middle of a word.
:set linebreak
" The minus sign is a letter. Maybe not in coding languages, but in everyday
" languages at least and I don't want them to be cut.
:set iskeyword+=-
" The word mustn't be wrapped on /.
:set breakat-=/
" Neither on the dot which can be used in filenames
:set breakat-=.
" But they can be cut after a tag is closed.
:set breakat+=>
" To display a more visible wrapped line.
:set showbreak====>
" To display the last line even if not full, great with very long lines.
:set display=lastline
" It's new with vim8, to keep indentation with line wrapped.
:set breakindent

" END of wrapping lines ---------- }}}

" settings related to the French language/keyboard ----------- {{{

" I need something else than \ which is not easily reached on a French keyboard
let mapleader = "_"
let maplocalleader = ","

" To see the mistake I could make in writing French or English.
:setlocal spell spelllang=en,fr
:syntax spell toplevel

" To get only 5 lines when using z=
:set spellsuggest=5

" I'm French, I need more than 26 letters and I hate when either iso or the
" Microsoft encoding is set.
:set encoding=utf-8
:set fileencoding=utf-8
:set termencoding=utf-8

" With a French keyboard, the <C-]> is difficult to get, and for help file the
" Enter Key is useless by default, logical and very easy to use.
:autocmd FileType help noremap <buffer> <Enter> <C-]>

" END of French language/keyboard related settings --------------- }}}

" Settings depending on the terminal/GUI ---------------- {{{
" We begin with the colors which are common on Windows And Linux
" We don't chose a theme for vim in terminal because it uses the terminal
" colors
	" Settings of the GUI colors ------------------ {{{

	" Uncomment only the one used
	:set background=dark
	":colorscheme nord
	":colorscheme desert
	":colorscheme dracula
	":colorscheme elflord
	":colorscheme evening
	":colorscheme gruvbox
	":colorscheme hybrid
	":colorscheme industry
	":colorscheme jellybeans
	":colorscheme koehler
	":colorscheme molokai
	":colorscheme molokai_dark
	":colorscheme murphy
	":colorscheme oceandeep
	":colorscheme onedark
	":colorscheme pablo
	":colorscheme railscasts
	":colorscheme ron
	":colorscheme slate
	":colorscheme solarized
	":colorscheme sorcerer
	":colorscheme torte

	" END of the GUI colors' settings --------------- }}}
		" Settings of vim in a Linux terminal ----------- {{{
"if has("ttyout")
		" To be able to see all the colors of the terminal
		:let base16colorspace=256
		:set termguicolors
		:let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
		:let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
		" In Alacritty I need this to be able to advance word by word
		:map <ESC>[1;5A <C-Up>
		:map <ESC>[1;5B <C-Down>
		:map <ESC>[1;5C <C-Right>
		:map <ESC>[1;5D <C-Left>
		:imap <ESC>[1;5A <C-Up>
		:imap <ESC>[1;5B <C-Down>
		:imap <ESC>[1;5C <C-Right>
		:imap <ESC>[1;5D <C-Left>
		":colorscheme slate
		":colorscheme elflord
		":colorscheme solarized
		":colorscheme molokai
		:colorscheme molokai_dark
		":colorscheme gruvbox

		" It's new on the vim8, maybe I'll use it and put the colors outside
		" of the test. It's to be able to use the same colors as the GUI

		" To change the cursor depending on the mode
		if &term =~ "xterm\\|rxvt"
		  " use an green cursor in insert mode
		  let &t_SI = "\<Esc>]12;green\x7"
		  " use a red cursor otherwise
		  let &t_EI = "\<Esc>]12;red\x7"
		  silent !echo -ne "\033]12;red\007"
		  " reset cursor when vim exits
		  autocmd VimLeave * silent !echo -ne "\033]112\007"
		  " use \003]12;gray\007 for gnome-terminal and rxvt up to version 9.21
		endif

		" END of vim in a Linux terminal settings ---------------- }}}
"endif


" Must be set after the colors are defined to be sure to have what we want.
:syntax enable

" end settings of the terminal display }}}

" To write nicely the files --------------------- {{{

" To save the filename and the modification date systematically if it's there
" Remove the spaces at the end of the lines and put the cursor back where it
" was if the file isn't closed
" The + instead of / in the Fichier is for avoid issues with / in directories
:au BufWritePre,FileWritePre *sh*,*vim*,*tex*,*py,*conf* ks|call EnregFich()|'s
:fun! EnregFich()
:	silent! exe ":1,5s+Fichier :.*+Fichier : " . fnameescape(expand("%:t")) . "+"
:	silent! exe ":1,5s/Modif :.*/Modif : ".strftime("%a %d %b %Y %H:%M")."/"
:	silent! exe ":%s/\\s\\+$//"
":	silent! exe ":%s/\\(-- \\)\\@\!\s\\+$//"
:endfun

" End of the files writing ----------------- }}}

" Unrelated settings ----------------------- {{{

" Really helpful in command line to display the files/folders
:set wildmenu

" I've never encountered a command for which a timeoutlen would be nice.
" Si I put a huge value to have time to remember new/not often used commands
:set timeoutlen=10000

" everybody prefer spaces to tabs, but I prefer tabs. So As I'm the one using
" my config file, I'm using tabs. That's what's great with vim : one can do
" what one prefer without regarding other's preferences
:set tabstop=4
:set shiftwidth=4
:set noexpandtab

" I don't understand why I need it at the end, maybe a plugin which define the
" same keys. It's great to be able to change some config files easily
:nnoremap <leader>ev :split $MYVIMRC<cr>
:nnoremap <leader>eb :split ~/.bashrc<cr>
:nnoremap <leader>ei :split ~/.config/i3/config<cr>
:nnoremap <leader>ep :split ~/.config/polybar/config<cr>

" END of unrelated settings --------------------- }}}

" vim: ts=4 sw=4 noet
