#############################################################
#  Auteur : Stéphane CARPENTIER
#  Fichier : .bashrc
#    Modif : sam. 03 août 2024 10:28
#
#############################################################

# The package lines ----------------------------------------- {{{

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# End of the package lines ---------------------------------- }}}

# Aliases --------------------------------------------------- {{{

# I prefer the colors
alias tree='tree -C'

# I'm not yet decided for the ls replacement to display the colors
#alias ls='exa'
#alias ll='exa -al'
#alias lc='exa -las=modified'
#alias ls='colorls'
#alias ll='colorls -Al'
#alias lc='colorls -Altr'
alias ls='lsd --hyperlink=auto'
alias ll='ls -Al'
alias lc='ls -Altr'

# To get a more complete ps when needed
alias psc='ps axwf -eo ppid,pid,user,cgroup,pidns,args'

# To get a better vision of my disk usage
alias du="dust"

# When I have a pdf in qutebrowser I have to download it to see
# The both lines are to be able to look at it and removing it without bothering with it's name
# It's probably better to have a shortcut for this in sway, I'll see that in another step
alias zd="/bin/ls -tr ~/Downloads/ | tail -1 | xargs -I % zathura ~/Downloads/%"
alias md="/bin/ls -tr ~/Downloads/ | tail -1 | xargs -I % mpv ~/Downloads/%"
alias fd="/bin/ls -tr ~/Downloads/ | tail -1 | xargs -I % feh ~/Downloads/%"
alias rd="/bin/ls -tr ~/Downloads/ | tail -1 | xargs -I % rm ~/Downloads/%"
alias sd='/bin/ls -tr ~/Downloads/ | tail -1 | xargs -I % mv ~/Downloads/%'
alias sda='/bin/ls -tr ~/Downloads/ | tail -1 | xargs -I % mv ~/Downloads/% ~/Lecture/Pdf/ALire/'

# To easily update my system
alias us="~/update.sh"

# To disable wifi at home to be sure only the cable is used
alias wd="iwctl station wlan0 disconnect freebox_SC_FL"

# To manage my dotfiles
alias dtf='/usr/bin/git --git-dir=/home/stef/.dtf/ --work-tree=/home/stef'

# To use nix with experimental commands
alias nix="nix --extra-experimental-features nix-command "

# End of aliases --------------------------------------------- }}}

# Powerline settings ---------------------------------------- {{{

# Interesting information displayed in a colored way
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1

. /usr/share/powerline/bindings/bash/powerline.sh

# End of Powerline settings --------------------------------- }}}

# Information displayed at the terminal start --------------- {{{

# Once ready display information
neofetch
#fastfetch

# Then for a little fun short jokes
cowthink -f eyes -s $(fortune -as)

# End of displayed programs ---------------------------------- }}}

# To use Vagrant --------------------------------------------- {{{
export VAGRANT_DEFAULT_PROVIDER=libvirt
# End to use Vagrant --------------------------------------------- }}}


# Ugly things to clarify later ------------------------------- {{{
# Don't know why it doesn't work in i3/config a why it's not launched in .xinitrc
#xmodmap ~/.Xmodmap
# End of Ugly lines ------------------------------------------ }}}
# to enable fzf with [CTRL] + [R]
eval "$(fzf --bash)"

# vim: ts=4 sw=4 noet
export GUIX_LOCPATH=/home/stef/.guix-profile/lib/locale
